package com.demo.springboot.dto;

import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    public Integer getLastId(){
        return movies.get(movies.size()-1).getMovieId();
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public boolean updateMovie(Integer id, UpdateMovieDto update){
        boolean state = false;
        for (MovieDto movie: movies) {
            if(movie.getMovieId().equals(id)){
                movie.setTitle(update.getTitle());
                movie.setYear(update.getYear());
                movie.setImage(update.getImage());
                state = true;
            }
        }
        return state;
    }

    public boolean addMovie(CreateMovieDto createMovie){
        boolean state = false;
        int lastId = movies.get(movies.size()-1).getMovieId();
        if(createMovie.getTitle().length()>0 && createMovie.getYear()>0 && createMovie.getImage().length()>0){
            movies.add(new MovieDto(lastId+1,createMovie.getTitle(), createMovie.getYear(), createMovie.getImage()));
            state = true;
        }
        return state;
    }

    public boolean removeMovie(Integer id) {
        boolean state = false;
        for (MovieDto movie: movies) {
            if(movie.getMovieId().equals(id)){
                movies.remove(movie);
                state = true;
            }
        }
        return state;
    }

    @Override
    public String toString() {
        return "[" + movies.stream()
                .map(movie -> movie.getMovieId().toString())
                .collect(Collectors.joining(",")) + "]";
    }
}
