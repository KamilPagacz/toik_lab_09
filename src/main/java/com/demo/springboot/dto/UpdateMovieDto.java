package com.demo.springboot.dto;

public class UpdateMovieDto {
    private String title;
    private Integer year;
    private String image;

    public UpdateMovieDto() {
    }

    public String getTitle() {
            return title;
        }

    public Integer getYear() {
            return year;
        }

    public String getImage() {
            return image;
        }
}
