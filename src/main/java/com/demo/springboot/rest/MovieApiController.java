package com.demo.springboot.rest;

import com.demo.springboot.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);
    private final MovieListDto movies = new MovieContainerDto().getMovies();

    public MovieApiController() {

    }

    @GetMapping("/api/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movies);
        return ResponseEntity.ok().body(movies);
    }

    @DeleteMapping("/api/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable("id") Integer id) {
        LOG.info("--- id: {}", id);

        if(movies.removeMovie(id)){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/api/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable("id") Integer id, @RequestBody UpdateMovieDto updatemovieDto) {
        LOG.info("--- id: {}", id);
        LOG.info("--- title: {}", updatemovieDto.getTitle());
        LOG.info("--- year: {}", updatemovieDto.getYear());
        LOG.info("--- image: {}", updatemovieDto.getImage());

        if(movies.updateMovie(id, updatemovieDto)){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/api/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- title: {}", createMovieDto.getTitle());
        LOG.info("--- year: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());

        if(movies.addMovie(createMovieDto)){
            return ResponseEntity.created(new URI("/movies/" + movies.getLastId())).build();
        }else{
            return ResponseEntity.badRequest().build();
        }
    }
}
